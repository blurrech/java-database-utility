# Java Database Utility
This project uses the Apache Commons library to parse a CSV file, before it is imported into a MySQL Database using the MySQL-Java JDBC.

## Problem Overview
Attached is a CSV file containing jobs for a driver's route. The job lines are duplicated on some rows as there are multiple items being delivered on the drop. 

We would like the candidate to write a Java utility to read in the CSV and import the data into a database, preferably MySQL. 

Jobs should go into a jobs table and the items should go into an items table with a link to the job. 

Code should be uploaded into a source control management tool such as Gitlab and a link provided for review. 

A ReadMe file should be provided with instructions to build and run the code.

A nice to have would be a simple front end page to view the data imported.

We will be looking at the style and readability of the code, use of object oriented design principles and any unit tests provided with the code. 

## Dependencies

These are included in the /lib/ directory. They are listed for informative purposes only.

* [Apache Commons CSV Library](https://commons.apache.org/proper/commons-csv/)

* [JUnit 4 which includes Hamcrest Core](https://github.com/junit-team/junit4/wiki/download-and-install)

* [MySQL JDBC driver](http://dev.mysql.com/downloads/connector/j/)

# Setup Instructions

Note: these instructions were tested on a Windows machine.

## Preparations

* Ensure that you have a MySQL Server running at the host/target IP address. These should match either the default configuration or the command line arguments used.

* The Java program was built using [JDK1.8.0_181](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

* Ensure the JRE is present in the PATH environment variable. 

* If it isn't set, this can be fixed with a command similar to `set PATH=%PATH%;C:\Program Files\Java\jdk1.8.0_181\bin`

## Compile and Run

* First, navigate to the root folder of the project.

* Run the command `javac -d bin -cp "lib/*" src/main/DatabaseUtility.java` to compile.

* Next, enter the bin directory with `cd bin`

* Finally, run the program with `java -cp ../lib/*;. main/DatabaseUtility`

* To run with custom database details, run `java -cp ../lib/*;. main/DatabaseUtility [server_url] [username] [password]`

* For example, `java -cp ../lib/*;. main/DatabaseUtility jdbc:mysql://localhost:3306/ admin password`

* If providing arguments, all three must be given.

# Database Front-end

* The database front-end is available [here.](http://paulroskell.com/project/java-db-utility/)	

* This was imported using the command `java -cp ../lib/*;. main/DatabaseUtility jdbc:mysql://160.153.128.46:3306/ server_admin password`. If the MySQL host is remote, the `default_database` table should already exist.

* 160.153.128.46 is the IP for my hosting server http://paulroskell.com

* This stage can also be done manually in three steps:

 1. The database was exported using the command `mysqldump -u [admin] -p[password] [local_database_name] > exportedjobdata.sql`

 2. The SQL output was prepended with `USE [server_database_name]`

 3. Finally, the phpMyAdmin import tool was used to copy the database tables to the hosted server.