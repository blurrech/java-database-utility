-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: default_database
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `csv_data`
--

USE default_database;

DROP TABLE IF EXISTS `csv_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `csv_data` (
  `Company` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DC` int(11) DEFAULT NULL,
  `DeliveryDate` date DEFAULT NULL,
  `VehicleReg` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DropNo` int(11) DEFAULT NULL,
  `DropReference` int(11) DEFAULT NULL,
  `Timeslot` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomersName` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine1` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine2` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine3` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine4` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine5` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Postcode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DeliveryInstructions` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DropType` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StockCode` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  `StorePhoneNo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone1` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone2` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone3` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StoreCustomerDrop` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GoodsDescription` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RouteNumber` bigint(20) DEFAULT NULL,
  `RouteDesc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TakePhoto` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TomTom` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RequiresAssembly` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConcordeRef` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Barcode` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csv_data`
--

LOCK TABLES `csv_data` WRITE;
/*!40000 ALTER TABLE `csv_data` DISABLE KEYS */;
INSERT INTO `csv_data` VALUES ('SLMS',904,'2017-02-08','691',1,8323199,'PM','RAJESH PATEL','54 Inkerman Street','WOLVERHAMPTON','West Midlands','','','WV10 0EP','PLEASE CALL 30MINUTES BEFORE','D','436100004',1,'01217 725997','','','07429379505','C','150CM MATT MINNESOTA',9041793691,'91','N','N','N','XB2521459','436100004'),('SLMS',904,'2017-02-08','691',2,8323197,'PM','JAMES BRADLEY','4 Ashwood Avenue','STOURBRIDGE','West Midlands','','','DY8 5DE','PLEASE GIVE 30 MIN PRE CALL PLEASE','D','435000015',1,'01384261268','','','07871248652','C','150CM P/T 4 DRW SET MARLOW BACKCARE',9041793691,'91','Y','N','Y','XA5386046','435000015'),('SLMS',904,'2017-02-08','691',2,8323198,'PM','JAMES BRADLEY','4 Ashwood Avenue','STOURBRIDGE','West Midlands','','','DY8 5DE','PLEASE GIVE 30 MIN PRE CALL PLEASE','D','JCO150136',1,'01384261268','','','07871248652','C','KING MATT PROTECTOR MATT PRO TEFLON DEEP SKIRT',9041793691,'91','N','N','Y','XA5386046','JCO150136'),('SLMS',904,'2017-02-08','691',3,8323194,'PM','JAMES JONES','291 Gayfield Avenue','BRIERLEY HILL','West Midlands','','','DY5 2ST','please give precall COLLECT LOAN BED A7772319','D','428600255',1,'01384261268','','','07981643329','C','120CM P/T 2 DRW SET TRANQUIL COAL',9041793691,'91','Y','N','Y','XA5386011','428600255'),('SLMS',904,'2017-02-08','691',3,8323195,'PM','JAMES JONES','291 Gayfield Avenue','BRIERLEY HILL','West Midlands','','','DY5 2ST','please give precall COLLECT LOAN BED A7772319','D','POW135100',1,'01384261268','','','07981643329','C','DOUBLE DUVET ALL SEASONS 9+4.5 TOG',9041793691,'91','N','N','Y','XA5386011','POW135100'),('SLMS',904,'2017-02-08','691',3,8323196,'PM','JAMES JONES','291 Gayfield Avenue','BRIERLEY HILL','West Midlands','','','DY5 2ST','please give precall COLLECT LOAN BED A7772319','D','303500029',1,'01384261268','','','07981643329','C','120CM H/B ORLEANS BLACK FAUX LEATHER',9041793691,'91','N','N','Y','XA5386011','303500029'),('SLMS',904,'2017-02-08','691',4,8323187,'PM','JOE BLOGGS','25 Lissimore Drive','TIPTON','West Midlands','','','DY4 7SX','please call b4 arrival','D','398500034',1,'01215054308','01212941208','','07714096478','C','135CM P/T 2 DRW SET SENSAFORM 9000 SUMMER HAY',9041793691,'91','Y','N','Y','XB2497267','398500034'),('SLMS',904,'2017-02-08','691',4,8323188,'PM','JOE BLOGGS','25 Lissimore Drive','TIPTON','West Midlands','','','DY4 7SX','please call b4 arrival','D','BEN135079',1,'01215054308','01212941208','','07714096478','C','135CM H/B ELENA SHINY NICKEL',9041793691,'91','N','N','Y','XB2497267','BEN135079'),('SLMS',904,'2017-02-08','691',4,8323189,'PM','JOE BLOGGS','25 Lissimore Drive','TIPTON','West Midlands','','','DY4 7SX','please call b4 arrival','D','KAYPIL005',2,'01215054308','01212941208','','07714096478','C','SINGLE PILLOW SENSATION',9041793691,'91','N','N','Y','XB2497267','KAYPIL005'),('SLMS',904,'2017-02-08','691',4,8323190,'PM','JOE BLOGGS','25 Lissimore Drive','TIPTON','West Midlands','','','DY4 7SX','please call b4 arrival','D','JCO135135',1,'01215054308','01212941208','','07714096478','C','DOUBLE MATT PROTECTOR MATT PRO TELFON DEEP SKIRT',9041793691,'91','N','N','Y','XB2497267','JCO135135'),('SLMS',904,'2017-02-08','691',5,8323184,'PM','AMANDA JOHNSON','The Water Wheel Comberton Road','KIDDERMINSTER','Worcestershire','','','DY10 4AA','please give pre call COLLECT LOAN BED A7772303','D','410700075',1,'01562 747833','','07507860094','07976691436','C','150CM MATT HEIRLOOM MEDIUM',9041793691,'91','N','N','Y','XA3738907','410700075'),('SLMS',904,'2017-02-08','691',5,8323185,'PM','AMANDA JOHNSON','The Water Wheel Comberton Road','KIDDERMINSTER','Worcestershire','','','DY10 4AA','please give pre call COLLECT LOAN BED A7772303','D','KAYPIL001',1,'01562 747833','','07507860094','07976691436','C','SINGLE PILLOW IGEL BLISS SIDE SLEEPER',9041793691,'91','N','N','Y','XA3738907','KAYPIL001'),('SLMS',904,'2017-02-08','691',5,8323186,'PM','AMANDA JOHNSON','The Water Wheel Comberton Road','KIDDERMINSTER','Worcestershire','','','DY10 4AA','please give pre call COLLECT LOAN BED A7772303','D','DUN000027',1,'01562 747833','','07507860094','07976691436','C','NO SIZE PILLOW SERENITY',9041793691,'91','N','N','Y','XA3738907','DUN000027'),('SLMS',904,'2017-02-08','691',6,8323183,'PM','IAN BEALE','27 Mayfair Drive','Fazeley','TAMWORTH','Staffordshire','','B78 3TG','please call 30 mins before del','D','431500002',1,'01827 54111','','','07980966904','C','90CM MATT AMETHYST PKT DELUXE',9041793691,'91','N','N','N','XA5179467','431500002'),('SLMS',904,'2017-02-08','691',7,8323182,'PM','DALE JOHN','1 Ribblesdale','Wilnecote','TAMWORTH','Warwickshire','','B77 4LQ','pls call 30 mins b4 deli & take to room of choice','D','435100012',1,'01827 54111','01827331171','','','C','150CM P/T 2 DRW SET MARLOW BACKCARE COM',9041793691,'91','Y','N','Y','XA5179503','435100012'),('SLMS',904,'2017-02-08','691',8,8323179,'PM','DOREEN SMITH','BENSONS WITHIN HARVEYS','UNIT E2 RAVENSIDE RETAIL PARK','KINGSBURY RD','ERDINGTON','','B24 9QB','Please call 30 mins before arrival.','D','HUBVEG006',1,'0121 3864591','','','07814243718','S','135CM BED FRAME VEGAS BLACK **FRONT OPENING OTTOMAN**',9041793691,'91','N','N','N','XB1182190','HUBVEG006'),('SLMS',904,'2017-02-08','691',8,8323180,'PM','DOREEN SMITH','BENSONS WITHIN HARVEYS','UNIT E2 RAVENSIDE RETAIL PARK','KINGSBURY RD','ERDINGTON','','B24 9QB','Please call 30 mins before arrival.','D','382800034',1,'0121 3864591','','','07814243718','S','135CM MATT IGEL PEGASUS PLATINUM',9041793691,'91','N','N','N','XB1182190','382800034'),('SLMS',904,'2017-02-08','691',8,8323181,'PM','DOREEN SMITH','BENSONS WITHIN HARVEYS','UNIT E2 RAVENSIDE RETAIL PARK','KINGSBURY RD','ERDINGTON','','B24 9QB','Please call 30 mins before arrival.','D','SEA135782',1,'0121 3864591','','','07814243718','S','135CM MATT ALDERLEY BACKCARE',9041793691,'91','N','N','N','XB1182190','SEA135782');
/*!40000 ALTER TABLE `csv_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `DropReference` int(11) NOT NULL,
  `GoodsDescription` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StockCode` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  `TakePhoto` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RequiresAssembly` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConcordeRef` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Barcode` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`DropReference`),
  KEY `ConcordeRef` (`ConcordeRef`),
  CONSTRAINT `items_ibfk_1` FOREIGN KEY (`ConcordeRef`) REFERENCES `jobs` (`concorderef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (8323179,'135CM BED FRAME VEGAS BLACK **FRONT OPENING OTTOMAN**','HUBVEG006',1,'N','N','XB1182190','HUBVEG006'),(8323180,'135CM MATT IGEL PEGASUS PLATINUM','382800034',1,'N','N','XB1182190','382800034'),(8323181,'135CM MATT ALDERLEY BACKCARE','SEA135782',1,'N','N','XB1182190','SEA135782'),(8323182,'150CM P/T 2 DRW SET MARLOW BACKCARE COM','435100012',1,'Y','Y','XA5179503','435100012'),(8323183,'90CM MATT AMETHYST PKT DELUXE','431500002',1,'N','N','XA5179467','431500002'),(8323184,'150CM MATT HEIRLOOM MEDIUM','410700075',1,'N','Y','XA3738907','410700075'),(8323185,'SINGLE PILLOW IGEL BLISS SIDE SLEEPER','KAYPIL001',1,'N','Y','XA3738907','KAYPIL001'),(8323186,'NO SIZE PILLOW SERENITY','DUN000027',1,'N','Y','XA3738907','DUN000027'),(8323187,'135CM P/T 2 DRW SET SENSAFORM 9000 SUMMER HAY','398500034',1,'Y','Y','XB2497267','398500034'),(8323188,'135CM H/B ELENA SHINY NICKEL','BEN135079',1,'N','Y','XB2497267','BEN135079'),(8323189,'SINGLE PILLOW SENSATION','KAYPIL005',2,'N','Y','XB2497267','KAYPIL005'),(8323190,'DOUBLE MATT PROTECTOR MATT PRO TELFON DEEP SKIRT','JCO135135',1,'N','Y','XB2497267','JCO135135'),(8323194,'120CM P/T 2 DRW SET TRANQUIL COAL','428600255',1,'Y','Y','XA5386011','428600255'),(8323195,'DOUBLE DUVET ALL SEASONS 9+4.5 TOG','POW135100',1,'N','Y','XA5386011','POW135100'),(8323196,'120CM H/B ORLEANS BLACK FAUX LEATHER','303500029',1,'N','Y','XA5386011','303500029'),(8323197,'150CM P/T 4 DRW SET MARLOW BACKCARE','435000015',1,'Y','Y','XA5386046','435000015'),(8323198,'KING MATT PROTECTOR MATT PRO TEFLON DEEP SKIRT','JCO150136',1,'N','Y','XA5386046','JCO150136'),(8323199,'150CM MATT MINNESOTA','436100004',1,'N','N','XB2521459','436100004');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jobs` (
  `Company` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DC` int(11) DEFAULT NULL,
  `DeliveryDate` date DEFAULT NULL,
  `VehicleReg` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DropNo` int(11) DEFAULT NULL,
  `Timeslot` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomersName` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine1` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine2` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine3` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine4` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AddressLine5` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Postcode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DeliveryInstructions` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DropType` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StorePhoneNo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone1` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone2` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CustomerPhone3` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StoreCustomerDrop` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RouteNumber` bigint(20) DEFAULT NULL,
  `RouteDesc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TomTom` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConcordeRef` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ConcordeRef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES ('SLMS',904,'2017-02-08','691',5,'PM','AMANDA JOHNSON','The Water Wheel Comberton Road','KIDDERMINSTER','Worcestershire','','','DY10 4AA','please give pre call COLLECT LOAN BED A7772303','D','01562 747833','','07507860094','07976691436','C',9041793691,'91','N','XA3738907'),('SLMS',904,'2017-02-08','691',6,'PM','IAN BEALE','27 Mayfair Drive','Fazeley','TAMWORTH','Staffordshire','','B78 3TG','please call 30 mins before del','D','01827 54111','','','07980966904','C',9041793691,'91','N','XA5179467'),('SLMS',904,'2017-02-08','691',7,'PM','DALE JOHN','1 Ribblesdale','Wilnecote','TAMWORTH','Warwickshire','','B77 4LQ','pls call 30 mins b4 deli & take to room of choice','D','01827 54111','01827331171','','','C',9041793691,'91','N','XA5179503'),('SLMS',904,'2017-02-08','691',3,'PM','JAMES JONES','291 Gayfield Avenue','BRIERLEY HILL','West Midlands','','','DY5 2ST','please give precall COLLECT LOAN BED A7772319','D','01384261268','','','07981643329','C',9041793691,'91','N','XA5386011'),('SLMS',904,'2017-02-08','691',2,'PM','JAMES BRADLEY','4 Ashwood Avenue','STOURBRIDGE','West Midlands','','','DY8 5DE','PLEASE GIVE 30 MIN PRE CALL PLEASE','D','01384261268','','','07871248652','C',9041793691,'91','N','XA5386046'),('SLMS',904,'2017-02-08','691',8,'PM','DOREEN SMITH','BENSONS WITHIN HARVEYS','UNIT E2 RAVENSIDE RETAIL PARK','KINGSBURY RD','ERDINGTON','','B24 9QB','Please call 30 mins before arrival.','D','0121 3864591','','','07814243718','S',9041793691,'91','N','XB1182190'),('SLMS',904,'2017-02-08','691',4,'PM','JOE BLOGGS','25 Lissimore Drive','TIPTON','West Midlands','','','DY4 7SX','please call b4 arrival','D','01215054308','01212941208','','07714096478','C',9041793691,'91','N','XB2497267'),('SLMS',904,'2017-02-08','691',1,'PM','RAJESH PATEL','54 Inkerman Street','WOLVERHAMPTON','West Midlands','','','WV10 0EP','PLEASE CALL 30MINUTES BEFORE','D','01217 725997','','','07429379505','C',9041793691,'91','N','XB2521459');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-06 16:29:29
