<?php
if (isset($_POST["concorde_ref"])) {
  $table = "items";
} else {
  if (isset($_POST["table"])) {
    $table = $_POST["table"];
  } else {
    $table = "jobs";
    $_POST["table"] = $table;
  }
}
?>
<html>

<head>
<style>
h1 {
    text-align: center;
    font-family: Cambria;
    box-shadow: inset 0 0px 0 white, inset 0 -1px 0 black;
    padding-bottom: 10px;
}
h2 select, input {
    color: inherit;
    background: inherit;
    border: 1px solid black;
    padding: 6px;
}
select, input {
    display: block;
    margin: 0 10px 20px 10px;
    float: left;
}
table {
    border-collapse: collapse;
    margin: 0px auto;
}
table, th, td {
    border: 1px solid black;
    padding: 8px;
}
th {
    background-color: lightyellow;
}
</style>

</head>

<body>

<h1>Database View</h1>

<form method="post">
<h2><select name="table" onchange="this.form.submit();">
  <option value="jobs" 
<?php if ($table == "jobs") { echo "selected"; } ?>
>Jobs Table</option>
  <option value="items" 
<?php if ($table == "items") { echo "selected"; } ?>
><h2>Items Table</option>
</select></h2>
</form>

<form method="post" action="">
  <input type="hidden" name="drop_table" value="drop_table" />
  <input type="submit" value="Drop Tables" name="droptable" />
</form>
<table id="output">

<?php
function outputTable($tableName) {
  echo '<script type="text/javascript">', 'document.getElementById("output").innerHTML = "";', '</script>';

$host = "localhost";
$database_name = "default_database";
$username = "server_admin";
$password = "password";

$dbConnection = mysqli_connect($host, $username, $password, $database_name) or die ("Could not connect to MySQL.");
$sql = "";
  if (isset($_POST["drop_table"])) {
    $sql = "DROP TABLE IF EXISTS csv_data;";
    mysqli_query($dbConnection, $sql) or die("The drop csv_data query failed.");
    $sql = "DROP TABLE IF EXISTS items;";
    mysqli_query($dbConnection, $sql) or die("The drop items query failed.");
    $sql = "DROP TABLE IF EXISTS jobs;";
    mysqli_query($dbConnection, $sql) or die("The drop jobs query failed.");
    unset($_POST["drop_table"]);
    echo "<h3>There is no data to show.</h3>";
  } else if (isset($_POST["concorde_ref"])) {
    $sql = "SELECT * FROM $tableName WHERE ConcordeRef = '" . $_POST["concorde_ref"] . "' ";
  } else {
    $sql = "SELECT * FROM $tableName";
  }
  $results = mysqli_query($dbConnection, $sql) or die("<h3>There is no data to show.</h3>");
  if (mysqli_num_rows($results) > 0) {
    echo "<table><tr>";
    foreach (array_keys(mysqli_fetch_assoc(mysqli_query($dbConnection, $sql))) as $value) {
      echo "<th>" . $value . "</th>";
    }
    if ($_POST["table"] === "jobs") { 
      echo "<th>Actions</th>"; 
    }
    echo "</tr>";

    $results = mysqli_query($dbConnection, $sql) or die("The 'SELECT * FROM $tableName' query failed. ConcordeRef: " . $_POST["concorde_ref"]);

    while($row = mysqli_fetch_assoc($results)) {
      echo "<tr>";
      foreach ($row as $value) {
        echo "<td >" . $value . "</td>";
      }
      if ($_POST["table"] === "jobs") { 
        echo '<td><form method="post" action="">
              <input type="hidden" name="concorde_ref" value="' . $row['ConcordeRef'] . '" />
              <input type="submit" value="View Items" name="view_items" />
            </form></td>'; 
      }
      echo "</tr>";
    }
    echo "</table>";
  }
  mysqli_close($dbConnection);
}
outputTable($table);
?>
</table>

</body>
</html>