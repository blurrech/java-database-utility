package main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 * A Java database utility class for importing CSV files into a MySQL database.
 * @author Paul
 */
public class DatabaseUtility {
	public static final String JOBS_CSV_PATH = "jobdata.csv";
	
	public static final String MYSQL_USERNAME = "admin";
	public static final String MYSQL_PASSWORD = "password";
	public static final String MYSQL_DB_NAME = "default_database";
	
	public static final String MYSQL_SERVER_URL = "jdbc:mysql://localhost:3306/";
	
	public static final String RAW_CSV_TABLE_NAME = "csv_data";
	public static final String JOBS_TABLE_NAME = "jobs";
	public static final String ITEMS_TABLE_NAME = "items";

//  These table columns are copied from the data as given. 
	public static final String ALL_TABLE_COLUMNS = "Company,DC,DeliveryDate,VehicleReg,DropNo,DropReference,Timeslot,CustomersName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,AddressLine5,Postcode,DeliveryInstructions,DropType,StockCode,Qty,StorePhoneNo,CustomerPhone1,CustomerPhone2,CustomerPhone3,StoreCustomerDrop,GoodsDescription,RouteNumber,RouteDesc,TakePhoto,TomTom,RequiresAssembly,ConcordeRef,Barcode";
//  The second contains the columns associated with a Job.
	public static final String JOBS_TABLE_COLUMNS = "Company,DC,DeliveryDate,VehicleReg,DropNo,Timeslot,CustomersName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,AddressLine5,Postcode,DeliveryInstructions,DropType,StorePhoneNo,CustomerPhone1,CustomerPhone2,CustomerPhone3,StoreCustomerDrop,RouteNumber,RouteDesc,TomTom,ConcordeRef";
//	The third contains the columns associated with an Item. The ConcordeRef foreign key to the Jobs table is present in the Items table.
//  Although DropReference was used as the primary key, choosing this over the barcode was arbitrary.
	public static final String ITEMS_TABLE_COLUMNS = "DropReference,GoodsDescription,StockCode,Qty,TakePhoto,RequiresAssembly,ConcordeRef,Barcode";
	
	/**
	 * The main function of the program.
	 * args[0] is the server URL, args[1] is the MySQL user-name and args[2] is the MySQL password.
	 * If incorrect arguments are passed, the program will default to hard-coded constants.
	 * @param args All passed command-line arguments. 
	 */
	public static void main(String[] args) {
		try {
			String rootServerURL = MYSQL_SERVER_URL;
			String username = MYSQL_USERNAME;
			String password = MYSQL_PASSWORD;
			
			if (args.length >= 3) {
				if (args[0] instanceof String && args[1] instanceof String && args[2] instanceof String) {
					rootServerURL = args[0];
					username = args[1];
					password = args[2];
				}
			}
			Connection dbConnection;
			if (rootServerURL.equals("jdbc:mysql://localhost:3306/")) {
			    /// Connect to create the database.
			    dbConnection = DriverManager.getConnection(rootServerURL + "mysql", username, password);
			    createMySQLDatabase(dbConnection, MYSQL_DB_NAME);
			    dbConnection.close();
			}
						
			/// Reconnect to create the tables.
			dbConnection = DriverManager.getConnection(rootServerURL + MYSQL_DB_NAME, username, password);

			/// Drop all tables, to prevent data duplication or worse.
			dropTable(dbConnection, RAW_CSV_TABLE_NAME);
			dropTable(dbConnection, ITEMS_TABLE_NAME);
			dropTable(dbConnection, JOBS_TABLE_NAME);
			
			/// Begin by importing the raw data into a MySQL table 'as is'.
			createMySQLTable(dbConnection, RAW_CSV_TABLE_NAME);
			apacheImportCSV(dbConnection, RAW_CSV_TABLE_NAME, JOBS_CSV_PATH);

			/// Finally, create the specific tables requested from the raw data table.
			createTableSubset(dbConnection, JOBS_TABLE_NAME, JOBS_TABLE_COLUMNS, RAW_CSV_TABLE_NAME);
			createTableSubset(dbConnection, ITEMS_TABLE_NAME, ITEMS_TABLE_COLUMNS, RAW_CSV_TABLE_NAME);
			
			/// Link the tables and make uniqueness assumptions clear.
			addConstraint(dbConnection, "PRIMARY_KEY", JOBS_TABLE_NAME, "ConcordeRef", "");
			addConstraint(dbConnection, "PRIMARY_KEY", ITEMS_TABLE_NAME, "DropReference", "");
			addConstraint(dbConnection, "FOREIGN_KEY", ITEMS_TABLE_NAME, "ConcordeRef", JOBS_TABLE_NAME + "(ConcordeRef)");
			
			/// Output the contents of the tables to the console.
			outputTable(dbConnection, RAW_CSV_TABLE_NAME);
			outputTable(dbConnection, JOBS_TABLE_NAME);
			outputTable(dbConnection, ITEMS_TABLE_NAME);

			dbConnection.close();
			
			System.out.println("The program ran to completion without an exception.");
		} catch (IOException e) {
			System.out.println("The CSV file could not be read or copied.");
		} catch (SQLException e) {
			System.out.println("There was an SQL error when importing the database.");
		} catch (Exception e) {
			System.out.println("The MySQL JDBC driver failed to connect. Check the server is running and any passed details are correct.");
		}
	}
	
	/**
	 * Provides an output of the requested table in the console.
	 * @param connection The MySQL DB connection.
	 * @param tableName The name of the table to be output.
	 * @throws SQLException
	 */
	public static void outputTable(Connection connection, String tableName) throws SQLException {
	    System.out.println("--- Outputting the " + tableName + " table. ---");
	    ResultSet res = connection.createStatement().executeQuery("SELECT * FROM " + tableName);
	    while (res.next()) {
		    for (int i = 1; i <= res.getMetaData().getColumnCount(); i++) {
		        if (i > 1) {
		        	System.out.print(",  ");
		        }
		        System.out.print(res.getString(i) + " " + res.getMetaData().getColumnName(i));
		    }
		    System.out.print("\n");
		}
	    System.out.print("--- Done --- \n\n");
	}
	
	/**
	 * Drops the requested table from the MySQL database.
	 * @param connection The MySQL DB connection.
	 * @param tableName The name of the table to be dropped.
	 * @throws SQLException
	 */
	public static void dropTable(Connection connection, String tableName) throws SQLException {
		connection.prepareStatement("DROP TABLE IF EXISTS " + tableName).execute();
		System.out.println("Dropped the " + tableName + " table.\n");
	}
	
	/**
	 * Inserts the CSV file line-by-line into the Database, to avoid having to modify --secure-file-priv.
	 * @param connection The MySQL DB connection.
	 * @param tableName The name of the table in which to insert the CSV data.
	 * @throws IOException
	 * @throws SQLException 
	 */
	public static void apacheImportCSV(Connection connection, String tableName, String filePath) throws IOException, SQLException {
		final PreparedStatement statement = connection.prepareStatement("INSERT INTO " 
					+ tableName + " (" + ALL_TABLE_COLUMNS + ") "
					+ "VALUES(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?)");
				
		CSVParser parser = CSVParser.parse(new File(filePath), Charset.defaultCharset(), CSVFormat.MYSQL);
		parser.iterator().next(); // Skip column headers
		
		for (CSVRecord record : parser) {
			final String[] values = record.get(0).split(",");
			for (int i = 0; i < values.length; i++) {
				statement.setString(i + 1, values[i]);
			}
			statement.addBatch();
		}
		statement.executeBatch();
	}
	
	/**
	 * Creates a MySQL database with a given name, if it doens't already exist.
	 * @param connection The MySQL DB connection.
	 * @param databaseName The name of the database to create.
	 * @throws SQLException 
	 */
	public static void createMySQLDatabase(Connection connection, String databaseName) throws SQLException {
		connection.prepareStatement("CREATE DATABASE IF NOT EXISTS " + databaseName + " DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;").execute();
	    System.out.println("MySQL Database " + databaseName + " was created or already exists.\n");
	}
	
	/**
	 * Creates a table for the CSV data to be loaded into.
	 * The data types are hard-coded.
	 * @param connection The MySQL DB connection.
	 * @param tableName The name of the table to create.
	 * @throws SQLException 
	 */
	public static void createMySQLTable(Connection connection, String tableName) throws SQLException {
	    final String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " ( Company VARCHAR(128), "
	        	+ "DC INT, "
	        	+ "DeliveryDate DATE, "
	        	+ "VehicleReg VARCHAR(128), "
	        	+ "DropNo INT, "
	       		+ "DropReference INT, "
	       		+ "Timeslot VARCHAR(2), "
	       		+ "CustomersName VARCHAR(128), "
	       		+ "AddressLine1 VARCHAR(128), "
	       		+ "AddressLine2 VARCHAR(128), "
	       		+ "AddressLine3 VARCHAR(128), "
	       		+ "AddressLine4 VARCHAR(128), "
	       		+ "AddressLine5 VARCHAR(128), "
	        	+ "Postcode VARCHAR(15), "
	        	+ "DeliveryInstructions VARCHAR(128), "
	       		+ "DropType CHAR, "
	       		+ "StockCode VARCHAR(128), "
	       		+ "Qty INT, "
	       		+ "StorePhoneNo VARCHAR(15), "
	       		+ "CustomerPhone1 VARCHAR(15), "
	       		+ "CustomerPhone2 VARCHAR(15), "
	       		+ "CustomerPhone3 VARCHAR(15), "
	       		+ "StoreCustomerDrop CHAR, "
        		+ "GoodsDescription VARCHAR(128), "	
	       		+ "RouteNumber BIGINT, "
	        	+ "RouteDesc VARCHAR(128), "
	        	+ "TakePhoto CHAR, "
	       		+ "TomTom CHAR, "
	       		+ "RequiresAssembly CHAR, "
	       		+ "ConcordeRef VARCHAR(128), "
	       		+ "Barcode VARCHAR(128));";
		connection.prepareStatement(sql).execute();
	    System.out.println("The " + tableName + " table was created or already exists.\n");
	}

	/**
	 * Creates a populated table as a subset of an existing table columns.
	 * @param connection The MySQL DB connection.
	 * @param subsetTableName The name of the table to create.
	 * @param subsetTableColumns The columns shared by the new and parent table.
	 * @param parentTableName The name of the table to copy data from.
	 * @throws SQLException
	 */
	public static void createTableSubset(Connection connection, String subsetTableName, String subsetTableColumns, String parentTableName) throws SQLException {
		connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + subsetTableName + " AS SELECT DISTINCT " + subsetTableColumns + " FROM " + parentTableName + ";").execute();
		System.out.println("Created the separate " + subsetTableName + " table.\n");
	}
	
	/**
	 * Adds a constraint to a specified table.
	 * @param connection The MySQL DB connection.
	 * @param constraintName The type of constraint to add.
	 * @param tableName The name of the table to constrain.
	 * @param columnName The name of the column to constrain.
	 * @param reference A constraint relevant table(column) reference.
	 * @throws SQLException
	 */
	public static void addConstraint(Connection connection, String constraintName, String tableName, String columnName, String reference) throws SQLException {
		String sql = "ALTER TABLE " + tableName;
		switch (constraintName) {
		case "PRIMARY_KEY":
			sql += " ADD PRIMARY KEY (" + columnName + ")";
			break;
		case "FOREIGN_KEY":
			sql += " ADD FOREIGN KEY (" + columnName + ") REFERENCES " + reference;
			break;
		}
		connection.prepareStatement(sql).execute();
	}

}
