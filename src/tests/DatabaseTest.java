package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

import main.DatabaseUtility;

/**
 * 
 * @author Paul
 *
 */
public class DatabaseTest {

	@Test
	public void dbConnect() {
		try {
			DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
		} catch (SQLException e) {
			fail("Expected the JDBC driver to connect to the database.");
		}
	}
	
	@Test
	public void dbConnectWrongUsername() {
		try {
			DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL, "Nowhere Man", DatabaseUtility.MYSQL_PASSWORD);
			fail("Expected an SQLException to be thrown.");
		} catch (SQLException e) { }
	}
	
	@Test
	public void dbConnectWrongURL() {
		try {
			DriverManager.getConnection("http://www.google.com", DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
			fail("Expected an SQLException to be thrown.");
		} catch (SQLException e) { }
	}
	
	@Test
	public void dbConnectWrongPassword() {
		try {
			DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, "unlock");
			fail("Expected an SQLException to be thrown.");
		} catch (SQLException e) { }
	}
	
	@Test
	public void dbCreateDatabase() {
		try {
			Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + "mysql", DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
			DatabaseUtility.createMySQLDatabase(dbConnection, DatabaseUtility.MYSQL_DB_NAME);
			dbConnection.close();
		} catch (SQLException e) {
			fail("Expected the JDBC driver to connect to the database.");
		}
	}
	
	@Test
	public void apacheImportCSV() {
		try {
			dbCreateDatabase();
			Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
			DatabaseUtility.createMySQLTable(dbConnection, DatabaseUtility.RAW_CSV_TABLE_NAME);
			DatabaseUtility.apacheImportCSV(dbConnection, DatabaseUtility.RAW_CSV_TABLE_NAME, DatabaseUtility.JOBS_CSV_PATH);
		} catch (SQLException e) {
			fail("SQLException should not have been thrown.");
		} catch (IOException e) {
			fail("IOException should not have been thrown.");
		}
	}
	
	@Test
	public void apacheImportCSVWrongPath() {
		try {
			dbCreateDatabase();
			Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
			DatabaseUtility.createMySQLTable(dbConnection, DatabaseUtility.RAW_CSV_TABLE_NAME);
			DatabaseUtility.apacheImportCSV(dbConnection, DatabaseUtility.RAW_CSV_TABLE_NAME, "C:\\Users\\jobdata.csv");
			fail("Expected an IOException.");
		} catch (SQLException e) {
			fail("SQLException should not have been thrown.");
		} catch (IOException e) {
		}
	}
	
	@Test
	public void dbOutputJobsTable() throws SQLException {
		dbCreateJobsTable();
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
	    DatabaseUtility.outputTable(dbConnection, DatabaseUtility.JOBS_TABLE_NAME);

	    String expectedOutput = "--- Done --- ";
	    String[] printedLines = outContent.toString().split("\n");
	    assertEquals(expectedOutput, printedLines[printedLines.length - 1]);
	}
	
	@Test
	public void dbCreateJobsTable() throws SQLException {
		apacheImportCSV();
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		
		Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
		DatabaseUtility.createTableSubset(dbConnection, DatabaseUtility.JOBS_TABLE_NAME, DatabaseUtility.JOBS_TABLE_COLUMNS, DatabaseUtility.RAW_CSV_TABLE_NAME);
		
	    String expectedOutput = "Created the separate jobs table.";
	    assertEquals(expectedOutput, outContent.toString().split("\n")[0]);
	}

	@Test
	public void dbCreateItemsTable() throws SQLException {
		apacheImportCSV();
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		
		Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
		DatabaseUtility.createTableSubset(dbConnection, DatabaseUtility.ITEMS_TABLE_NAME, DatabaseUtility.ITEMS_TABLE_COLUMNS, DatabaseUtility.RAW_CSV_TABLE_NAME);
		
	    String expectedOutput = "Created the separate items table.";
	    assertEquals(expectedOutput, outContent.toString().split("\n")[0]);
	}
	
	@Test
	public void dbDropJobsTable() throws SQLException {
	    dbDropItemsTable();
	    dbCreateJobsTable();
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
	    DatabaseUtility.dropTable(dbConnection, DatabaseUtility.JOBS_TABLE_NAME);

	    String expectedOutput = "Dropped the jobs table.";
	    assertEquals(expectedOutput, outContent.toString().split("\n")[0]);
	}
	
	@Test
	public void dbDropItemsTable() throws SQLException {
		dbCreateItemsTable();
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		Connection dbConnection = DriverManager.getConnection(DatabaseUtility.MYSQL_SERVER_URL + DatabaseUtility.MYSQL_DB_NAME, DatabaseUtility.MYSQL_USERNAME, DatabaseUtility.MYSQL_PASSWORD);
	    DatabaseUtility.dropTable(dbConnection, DatabaseUtility.ITEMS_TABLE_NAME);

	    String expectedOutput = "Dropped the items table.";
	    assertEquals(expectedOutput, outContent.toString().split("\n")[0]);
	}
	
}
